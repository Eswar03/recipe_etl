
mysql_insert_pp_user = ('''
    INSERT INTO pp_users(u, techniques, mean_ratings)
    VALUES (%s, %s, %s)
''')

mysql_insert_pp_interaction = ('''
    INSERT INTO interactions(recipe_id, date, rating, u, i)
    VALUES (%s, %s, %s, %s, %s)
''')

mysql_insert_raw_recipie = ('''
    INSERT INTO raw_recipes(id, name, minutes, contributor_id, submitted, description, 
            nutri_c1, nutri_c2, nutri_c3, nutri_c4, nutri_c5, nutri_c6, nutri_c7)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
''')


mysql_insert_ingredients = ('''
    INSERT INTO ingr_map(ingr_id, raw_ingr, replaced, count, id)
    VALUES (%s, %s, %s, %s, %s)
''')

mysql_insert_steps = ("""
    INSERT INTO steps(name, recipe_id)
    VALUES (%s, %s) 
""")

mysql_insert_tags = ("""
    INSERT INTO tags(name, tag_id)
    VALUES (%s, %s) 
""")

mysql_insert_ingredients = ("""
    INSERT INTO ingredients(name, replaced, count, id)
    VALUES (%s, %s, %s, %s) 
""")