import csv
import statistics
from datetime import datetime

from db_credentials import csv_sources
from variables import datawarehouse_name


class InteractionEtl:

    def __init__(self, csv_path, query, conn):
        self.csv_path = csv_path
        self.conn = conn
        self.cursor = self.conn.cursor()
        self.query = query
        self.message_errors = []

    def __extract_transform_load(self):

        with open(csv_sources[self.csv_path]) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                self.__transform(row)  # Transform

    def __transform(self, row):

        row["recipe_id"] = int(row["recipe_id"])
        row["u"] = int(row["u"])
        row["rating"] = float(row["rating"])
        row["i"] = int(row["i"])

        row_values = (row["recipe_id"], row["date"], row["rating"], row["u"], row["i"])

        self.__load_one_by_one(row_values)


    def __load_one_by_one(self, row):
        try:
            self.cursor.execute(self.query, row)

        except Error as error:
            self.message_errors.append(error)

        except Exception as e:
            print(e.args)

    def etl_process(self):

        self.__extract_transform_load()
        self.conn.commit()
        self.cursor.close()

