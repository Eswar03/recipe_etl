import csv
import statistics

from db_credentials import csv_sources
from variables import datawarehouse_name

class UserEtl:

    def __init__(self, csv_path, query, conn):
        self.csv_path = csv_path
        self.query = query
        self.conn = conn
        self.cursor = self.conn.cursor()
        self.users = []

    def __extract_transform(self):

        with open(csv_sources[self.csv_path]) as csv_file:

            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                self.__transform(row)   # Transform

    def __transform(self, row):

        del row["n_items"]
        del row["n_ratings"]
        del row["items"]

        row["mean_ratings"] =  round(statistics.mean([float(i) for i in row["ratings"][1:-1].split(",")]), 2)
        del row["ratings"]
        row["u"] = int(row["u"])

        row_values = (row["u"], row["techniques"], row["mean_ratings"])
        self.users.append(row_values)

    def __load(self):
        self.cursor.executemany(self.query, self.users)

    def etl_process(self):

        self.__extract_transform()
        self.__load()

        self.conn.commit()

        self.cursor.close()
