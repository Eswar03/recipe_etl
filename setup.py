 
from setuptools import find_packages, setup

setup(
    name='recipy_etl',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'mysql-connector-python'
    ],
)
